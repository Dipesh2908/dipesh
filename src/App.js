import React from 'react';
import { Carousel, Navbar,Nav,Container,Row,Col,Card,Button} from 'react-bootstrap';
import './App.css';

function App() {
  return (
    <div>
          <div>
          <Navbar collapseOnSelect expand="lg" bg="info" variant="dark" className="fixed-top">
                <Navbar.Brand href="#home">Dipesh Chaudhari</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="ml-auto">
      <Nav.Link href="#home">Home</Nav.Link>
      <Nav.Link href="#about">About</Nav.Link>
      <Nav.Link href="#education">Education</Nav.Link>
      <Nav.Link href="#portfolio">Portfolio</Nav.Link>
      <Nav.Link href="#experience">Experience</Nav.Link>
      <Nav.Link href="#contact">Contact</Nav.Link>
      
    </Nav>
    
  </Navbar.Collapse>
</Navbar>
          </div>
          <section id="home">
      <Carousel>
        <Carousel.Item>
             <img
            className="d-block w-100"
              src="https://cdn.pixabay.com/photo/2018/03/20/20/15/laptop-3244483_960_720.jpg"  alt="First slide"
             />
    <Carousel.Caption className="ctt text-info">
      <p>Web Designer</p>
      <h2>Ability to build a responsive website.</h2>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://cdn.pixabay.com/photo/2014/05/02/21/49/home-office-336373_960_720.jpg"
      alt="Third slide" 
    />

    <Carousel.Caption className="ctt text-info">
      <p>Software Developer</p>
      <h2>Ability to build a software .</h2>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://cdn.pixabay.com/photo/2014/05/03/01/03/macbook-336704_960_720.jpg"
      alt="Third slide"
    />

    <Carousel.Caption className="ctt text-info">
      <p>Digital Marketer</p>
      <h2>Ability to doing Digital Marketing.</h2>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
</section>


<section id="about" className="mt-5 mt-5">
<Container>
<br/><br/><br/>
  <hr className="linehr"></hr>
  <h2 className="text-center text-info">About</h2>
  <hr className="linehr"></hr>
  <Row>
    <Col><img src="https://cdn.pixabay.com/photo/2015/03/26/09/41/tie-690084_960_720.jpg" alt="" className="imgabout"/></Col>
    <Col className="abouttext">
    <p>M.Sc in Digital Marketing and sales from kedge business school, Marseille, France. With a background in Information Technology, looking for the Full time or part time or CDD or CDI opportunities, Looking to gain and enhance skills in the areas of Web Design, Responsive web, Web Development, Web marketing,
Web Analysis, Web testing, Search Engine Marketing, Digital Marketing, Social Media Marketing, Content Creation, and Video Production.</p>
    </Col>
  </Row> 
  
</Container>
</section>

<section id="education" className="mt-5">
<Container>
<br/><br/><br/>
  <hr className="linehr"></hr>
  <h2 className="text-center text-info">Education</h2>
  <hr className="linehr"></hr>
    <Row>
    <Col><h3>B.Sc Computer Science</h3>
    <ul>
   <li>  Fundamentals of Computer Organization & Embedded Systems</li>
   <li> Introduction to Database Management Systems</li>
   <li> Introduction to programming using Python</li>
   <li> Advanced Programming using Python</li>
   <li> Discrete Mathematics</li>
   <li> Computer Graphics</li>
   <li> C++  Programming</li>
   <li> Java Programming</li>
   <li> Database Management Systems</li>
   <li> Software Engineering</li>


    </ul>
    </Col>
    <Col><h3>M.Sc Computer Science</h3>
    <ul>
   <li>Advance Web Designing </li>
   <li> Computer Networks</li>
   <li>Information Security </li>
   <li>Cryptography </li>
   <li> Data Mining</li>
   <li> Computer Graphics</li>
   <li> Digital Image Processing & Multi-media</li>
   <li> Advance Java Programming</li>
   <li> Advance Database Management Systems</li>
   <li> Advance Software Engineering</li>
 
   </ul>
    </Col>
    <Col><h3>M.Sc Digital Marketing</h3> 
    <ul>
   <li>UX/UI Design and Thinking </li>
   <li> SocialMedia Marketing</li>
   <li> Advance SEO and SEA </li>
   <li> Video Marketing</li>
   <li> Data Analysis</li>
   <li> Computer Graphics</li>
   <li> Operations Management</li>
   <li> Human Resource Management</li>
   <li> Geopolitics & The World Economic System</li>
   <li> Business Systems & Procedures</li>
 
   </ul>
    </Col>
  </Row>
  
</Container>
</section>

<section id="portfolio" className="mt-5">

<Container>
<br/><br/><br/>
  <hr className="linehr"></hr>
<h2 className="text-center text-info">Portfolio</h2>
  <hr className="linehr"></hr>
    <Row>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2015/01/20/14/27/office-605503_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="https://dipesh.co.in" target="_blank">dipesh.co.in</Nav.Link></Col>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2018/03/01/09/33/laptop-3190194_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="https://www.ddigital1.com" target="_blank">ddigital1.com</Nav.Link></Col>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2015/02/05/08/06/macbook-624707_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="https://urvichaudhari.com/" target="_blank">urvichaudhari.com</Nav.Link></Col>
  </Row>
  <Row>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2016/11/29/09/27/electronics-1868708_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="https://www.instagram.com/p/ByAzn5BIpoM/?utm_source=ig_web_options_share_sheet" target="_blank">Etalian cook [logo]</Nav.Link></Col>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2017/06/17/06/08/laptop-2411303_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="#" target="_blank">Intreact Kedge</Nav.Link></Col>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2018/05/19/00/53/online-3412473_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="https://www.instagram.com/ddigital014/" target="_blank">Digital Marketing</Nav.Link></Col>
  </Row>
  <Row>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2019/09/09/08/23/internet-4463031_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="https://www.youtube.com/channel/UC3liztMptvUmF_-Kfyk_2BA" target="_blank">Video Marketing</Nav.Link></Col>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2015/06/24/15/45/ipad-820272_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="https://www.instagram.com/ddigital014/" target="_blank">Content Creation</Nav.Link></Col>
    <Col sm className="text-center"><img src="https://cdn.pixabay.com/photo/2016/11/19/21/01/analysis-1841158_960_720.jpg" alt="" className="imgpo"/><Nav.Link href="https://www.linkedin.com/in/dipeshchaudhari/" target="_blank">Social Media</Nav.Link></Col>
  </Row>
  <br/><br/><br/><br/>
</Container>
</section>



<section id="experience" className="mt-5">

<Container>
<br/><br/><br/>
  <hr className="linehr"></hr>
<h2 className="text-center text-info">Experience</h2>
  <hr className="linehr"></hr>
  <Row>
    <Col>
    <Card bg="primary" text="white" style={{ width: '18rem' }}>
    <Card.Header>Lecturer</Card.Header>
    <Card.Body>
      <Card.Title>Government Science College Valod, Tapi, India</Card.Title>
      <Card.Text>
        <ul>
           
        <li>Created lecture presentations for both online and in-class
environments.</li>
        <li>Designed course materials, syllabus, writing assignments and
exams</li>
        <li>Organized class events and activities to promote learning.</li>
        </ul>
       
      </Card.Text>
    </Card.Body>
  </Card>
    
    </Col>
    <Col>
    <Card bg="secondary" text="white" style={{ width: '18rem' }}>
    <Card.Header>Software Developer</Card.Header>
    <Card.Body>
      <Card.Title>Global Infotech, Surat, India</Card.Title>
      <Card.Text>
      <ul>
           
           <li>Designed intuitive graphical user interface and database
design.</li>
           <li>Implemented link building campaigns in coordination with
client SEO goals</li>
           <li>Provide SEO analysis and recommendations in coordination
with elements and structure of websites.</li>
           </ul>
      </Card.Text>
    </Card.Body>
  </Card>
    </Col>
    <Col>
    <Card bg="success" text="white" style={{ width: '18rem' }}>
    <Card.Header>Graphic Designer</Card.Header>
    <Card.Body>
      <Card.Title>Bosch, Rodez, France<br/><br/></Card.Title>
      <Card.Text>
      <ul>
           
           <li>UX/UI Design for Bosch Innovation Community.</li>
           <li>Creating Banner and post for the Bosch Community on the basis of Innovation thinking.</li>
           <li>Developing a responsive web site for Bsoch Innovation to give meaningfull Idea.</li>
           
           
           </ul>
      </Card.Text>
    </Card.Body>
  </Card>
    
    
    </Col>
  </Row>
  <br/><br/><br/><br/>
</Container>
</section>

<section id="contact" className="mt-5">
<container>

<Card className=" text-center">
  <Card.Header><h2 className="text-info">Contact</h2></Card.Header>
  <Card.Body>
    <Card.Title>Hire me If you are Intrested :)</Card.Title>
    <Card.Text>
      chaudhari.dipesh3360@gmail.com.
    </Card.Text>
    <Button variant="primary">Call Me</Button>
  </Card.Body>
  <Card.Footer className="text-muted">CopyRights© Dipesh Chaudhari </Card.Footer>
</Card>
</container>


</section>
    </div>
  );
}

export default App;
